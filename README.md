# user-app

## Installation

Run commands:
```
docker-compose up --b --d
docker-compose exec php-fpm bash
composer install
bin/console --no-interaction doctrine:migrations:migrate
bin/console --no-interaction doctrine:fixtures:load
```

## Usage

go to http://localhost:8000

user credentials

* username: **user**
* password: **pass123**

