<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    // ...
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; ++$i) {
            foreach ($this->getUserNames() as $userName) {
                $user = new User();
                $user->setUsername(($i > 0) ? $userName . $i : $userName);
                $password = $this->encoder->encodePassword($user, 'pass123');
                $user->setPassword($password);
                $user->setIsDisabled(($i % 2) ? true : false);

                $manager->persist($user);
            }
        }

        $manager->flush();
    }

    private function getUserNames(): array
    {
        return [
            'user', 'bob_marley', 'anna_kowalska', 'wojtek_strazak', 'kasia_pieta', 'johny_deep',
        ];
    }
}
