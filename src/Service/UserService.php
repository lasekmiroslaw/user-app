<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserService
{
    /** @var UserRepository */
    private $userRepository;

    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
    }

    public function disableUser(int $userId): void
    {
        $user = $this->userRepository->find($userId);
        if (!$user instanceof User) {
            throw new NotFoundHttpException('User not found');
        }

        $user->setIsDisabled(true);
        $this->entityManager->flush();
    }
}
