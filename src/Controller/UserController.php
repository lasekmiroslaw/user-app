<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/users/page/{page<[1-9]\d*>}", defaults={"page": "1", "_format"="html"}, methods={"GET"}, name="users")
     */
    public function getUsers(int $page, UserRepository $userRepository)
    {
        return $this->render('user/index.html.twig', [
            'paginator' => $userRepository->findPaginatedUsers($page),
        ]);
    }

    /**
     * @Route("/users/disable/{userId}", requirements={"id"="\d+"}, methods={"GET"}, name="user_disable")
     */
    public function disableUser(int $userId, UserService $userService)
    {
        $userService->disableUser($userId);
        $this->addFlash('success', "User with id $userId has been disabled");

        return $this->redirectToRoute('users');
    }
}
